

*********************************************************************

README FOR IPC
*********************************************************************


Minix Version : 3.2.1 
Compiler : cc 

Login Details for MINIX 3.2.1
**********************************************************************
login    : root
password : hello

Loading Program to minix:
**********************************************************************

1. Browse the .vmx file from VMware Player to open the Minix 3.2.1
2. Using any FTP client like FileZilla, transfer the below files to respective directories

callnr.h                                  > /usr/src/include/minix/

ErrorCodes.h, IPC, makefile(modified), 
handler.c, table.c, proto.h               > /usr/src/servers/pm/

shell.sh, MainFile.c                      > /Project2

3. Navigate to /usr/src/releasetools/ and "make install" in that directory to compile the kernel

4. Once kernel is compiled, reboot the system to make the system calls ready to use.

5. Launch the Program by using the provided shell.sh file.


Contents of the Zip FIle:
**********************************************************************
1.Vmware virtual machine image
2.source files


Execution
**********************************************************************

	  Current processId : processId

          1. Create mailbox
	  2. Deposit Message
	  3. Retrieve Message
	  4. Register Process to mailbox
	  5. Delete the mailbox
          6. Spawn Process
          7. Set Security Bit
          8. Exit/switch process
          9. Exit Program


> Current process Id is displayed above in the menu

> you can create a process by using option 6 or you can execute the program in another terminal by switching usinf ALT + F1 and ALT + F2.

> Create a Mailbox using option 1. A process p1 who creates a mailbox, becomes admin for the mailbox

> An admin can grant permission to others, deposit a message, retrieve a message and delete a mailbox. Also he can restrict the security

  settings of the mailbox by using set security bit system call

> A non admin can only retrieve the messages from mailbox. 

> All messages are garbage collected/ deleted once they are retrieved.

> Option 8 is used to switch to parent process if we create a new process using option 6

> Option 9 is used to exit the program