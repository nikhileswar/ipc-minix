#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/wait.h>
#include "ErrorCodes.h"
#include "IPC.h"

message m;

int CREATEMAILBOX(int MboxId, pid_t processId){

    //message m;
    
    m.mbox_id= MboxId;
    m.p_Sprocessid=processId;
    return(_syscall(PM_PROC_NR, PM_CREATEMAILBOX, &m)); 
}

int DEPOSITMESSAGE(char *msg,int MboxId, pid_t SprocessId,pid_t RprocessId){

  //message m;
  strncpy(m.m3_ca1,msg,M3_STRING);
  m.mbox_id=MboxId;
  m.p_Sprocessid=SprocessId;
  m.p_Rprocessid=RprocessId;
  return(_syscall(PM_PROC_NR, PM_DEPOSITMSG, &m));

}

int RETRIEVEMESSAGE(int MboxId, pid_t RprocessId){

  m.mbox_id=MboxId;
  m.p_Rprocessid=RprocessId;
  int retrn = _syscall(PM_PROC_NR, PM_RETRIEVEMSG, &m);
  return retrn;

}

int DELETEMAILBOX(int MboxId, pid_t sProcessId)
{
 
   m.mbox_id=MboxId;
   m.p_Sprocessid=sProcessId;
   return(_syscall(PM_PROC_NR, PM_DELETEMAILBOX, &m));
}

int REGISTERMAILBOX(int MboxId, pid_t sProcessId,int pid)
{

   m.mbox_id=MboxId;
   m.p_Sprocessid=sProcessId;
   m.p_Rprocessid=pid;
   return(_syscall(PM_PROC_NR, PM_REGMAILBOX, &m));
}

int SETSECURITY(int MboxId, pid_t sProcessId,int securityBit)
{

   m.mbox_id=MboxId;
   m.p_Sprocessid=sProcessId;
   m.m_SecurityBit=securityBit;
   return(_syscall(PM_PROC_NR, PM_SETSECURITYMB, &m));
}


int fn_depositmsg(int MBId){
     char Buff[10];
    int DPid;
            	 printf("\nPlease Enter String to be Sent\n");

                 int input = scanf("%s",Buff);
                        printf("\nPlease Enter Destination Process Id\n");         
                        scanf("%d",&DPid);
            	 if(input == -1)
            	 {
            	       printf("\nError : Failed to Read String from Console \n");
            	     
            	 }
                           
            	 DEPOSITMESSAGE(Buff,MBId,getpid(),DPid);
                 return EC_OK;                  

}


int fn_spawnprocess(){

    int status;
    pid_t pid = fork();

    if (pid < 0)
    {
        printf("\nFork Error\n");
        return EC_FORK_ERROR;
    } 
    else 
    {
        waitpid(pid, &status, 0);
        printf("\nprocess is created with id: %d\n",getpid());
        return EC_OK;
    }

}

void fn_setsecurity(){

    int mailBoxId=0;
    int securitybit=0;
    int ret=0;
    printf("Enter Mailbox Id to set the security bit");
    scanf("%d",&mailBoxId);
    printf("Enter \n 0 for read-write \n 1 for read-only and \n 2 for write-only \n ");
    scanf("%d",&securitybit);
    ret = SETSECURITY(mailBoxId,getpid(),securitybit);

}


int main(){
    int option=0;
    int select=0;
    int mbId;
    char *name;
    int pid;
    int numMailbox=1000;
    
    while(option==0){
    
          printf("\n Current processId : %d",getpid());
          printf("\n		1. Create mailbox");
	  printf("\n		2. Deposit Message");
	  printf("\n		3. Retrieve Message");
	  printf("\n		4. Register Process to mailbox");
	  printf("\n		5. Delete the mailbox");
          printf("\n		6. Spawn Process");
          printf("\n		7. Set Security Bit");
          printf("\n		8. Exit/switch process");
          printf("\n		9. Exit Program");
	  printf("\n\n		Enter the choice: ");
	  scanf("%d",&select);
          switch(select){
          
              case 1: numMailbox++;
		      CREATEMAILBOX(numMailbox,getpid());
                      break;
              
              case 2: printf("\nEnter the mailbox ID to deposit: ");
	              scanf("%d",&mbId); 
                      fn_depositmsg(mbId);
                      break;
              
              case 3: printf("\nEnter the mailbox ID to retrieve: ");
                      scanf("%d",&mbId);
                      RETRIEVEMESSAGE(mbId,getpid());
                      break;
              
              case 4: printf("\nEnter existing mailbox ID to Register : ");
	              scanf("%d",&mbId);
                      printf("\nEnter Process Id to Register : ");
                      scanf("%d",&pid);
                      REGISTERMAILBOX(mbId,getpid(),pid);
                      break;
               
              case 5: printf("\nEnter the mailbox ID to be deleted : ");
	              scanf("%d",&mbId);
                      DELETEMAILBOX(mbId,getpid());
                      break;
              
              case 6: if(fn_spawnprocess()!=107){ }
                      break;
              case 7: fn_setsecurity();
                      break;  
              case 8: exit(0);
                      break;  
              case 9: option++;
              break;
          
          
          
          }
    
    }
    
    

}
