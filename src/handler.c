
#include "pm.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "IPC.h"
#include "proto.h"
#include "ErrorCodes.h"


int num_of_mailbox=0;

int do_CreateMailBox(){

    int mailboxId=0;
    pid_t processId;
    int index=0;
    int flag=0;
    mailboxId= m_in.mbox_id;
    processId=m_in.p_Sprocessid;
    
 for(index=0; index < num_of_mailbox; index++){
 
     if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
         flag++;
         break;
     }
 
 }
  
  if(index==NUMBER_OF_MAILBOX){
  
      printf("\n New Mailbox Cannot be Created. %d Mailboxes exist\n",num_of_mailbox);
      return EC_MAILBOX_LIMIT_EXCEEDED;
  
  }
  
  MasterDataStructure.Mailbox[index].MailBoxId=mailboxId;
  MasterDataStructure.Mailbox[index].ProcessInfo[0].processId=processId;
  MasterDataStructure.Mailbox[index].LatestProcess=MasterDataStructure.Mailbox[index].LatestProcess+1;
  num_of_mailbox++;
  printf("\n MailBox Created");
  printf("\nMailbox Id : %d\n",mailboxId);

  return EC_OK;

}


int do_DepositMsg()
{ 
    int mailboxId=0;
    pid_t SenderprocessId;
    pid_t RecieverprocessId;
    int index=0;
    int flag=0;
    int processflag=0;
    int pIndex;
    int Max_msg_index=0;
    int i=0;

    mailboxId= m_in.mbox_id;
    SenderprocessId=m_in.p_Sprocessid;
    RecieverprocessId=m_in.p_Rprocessid;

    for(index=0; index < num_of_mailbox; index++){

       if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
           flag++;
           break;
       }

   }
  
    if(flag==0){

          printf("\n Mail box does not exit\n");
          return EC_MAILBOX_DOESNT_EXIST;

    }
  
    for(pIndex=0; pIndex< MasterDataStructure.Mailbox[index].LatestProcess;pIndex++)
     {

        if(MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId==SenderprocessId){
            processflag++;
            break;
        }
  }
  if(processflag==0){
      printf("ProcessId %d Doesnt exist in Mailbox %d\n",SenderprocessId,mailboxId);
      return EC_PROCESS_DOESNT_EXIST;
  }
  
  Max_msg_index= MasterDataStructure.Mailbox[index].Max_msg_index;
 
  if( Max_msg_index < NUMBER_OF_MESSAGES){
       
      if(MasterDataStructure.Mailbox[index].security == 0 || MasterDataStructure.Mailbox[index].security == 2 ){ //check if we have permission to write in Mailbox
          for(i=0;i<MasterDataStructure.Mailbox[index].LatestProcess;i++){
          if(MasterDataStructure.Mailbox[index].ProcessInfo[i].processId==SenderprocessId){   // check if he is admin
            
              strncpy(MasterDataStructure.Mailbox[index].Message[Max_msg_index].Message, m_in.m3_ca1, M3_STRING-1);
              MasterDataStructure.Mailbox[index].Message[Max_msg_index].Sender=SenderprocessId;
              MasterDataStructure.Mailbox[index].Message[Max_msg_index].Reciever=RecieverprocessId;
              MasterDataStructure.Mailbox[index].isBlocked=0;
              MasterDataStructure.Mailbox[index].ProcessInfo[i].unread_Msg++;
              MasterDataStructure.Mailbox[index].Max_msg_index++;
              printf("\nMessage Deposited Successfully\n");
              return EC_OK;
          
          }
          
          }
		  
 
      }
      else{
      
          printf("\nMailbox is Write Protected. Aborting Operation\n ");
          return EC_WRITE_PROTECTED;
      
      }
  
  }
        else{
            printf("\n Mailbox Full. Cannot deposit any more Messages\n");
            MasterDataStructure.Mailbox[index].isBlocked=1;
            return EC_CANNOT_DEPOSIT;

        }
   
  
}
int do_RetrieveMsg()
{
   int mailboxId=0;
    pid_t SenderprocessId;
    pid_t RecieverprocessId;
    int index=0;
    int flag=0;
    int unreadMsg=0;
    int latestMsg;
    int msg_Index;
    int processflag=0;
    int pIndex;
    mailboxId= m_in.mbox_id;
    RecieverprocessId=m_in.p_Rprocessid;

  for(index=0; index < num_of_mailbox; index++){
     if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
         flag++;
         break;
     }
 
 }
  if(flag==0){
      
        printf("\n Mail box does not exit\n");
        return EC_MAILBOX_DOESNT_EXIST;
  
  }
  for(pIndex=0; pIndex< MasterDataStructure.Mailbox[index].LatestProcess;pIndex++)
   {
      if(MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId== RecieverprocessId){
          processflag++;
          break;
      }
}
             latestMsg=MasterDataStructure.Mailbox[index].Max_msg_index;
             unreadMsg= MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].unread_Msg;
             if(latestMsg==0){
                  printf("\n Mailbox is Empty. Cannot Read any more Messages\n");
                  MasterDataStructure.Mailbox[index].isBlocked=1;
                  return EC_CANNOT_RETRIEVE;
                 
                 
             }
              MasterDataStructure.Mailbox[index].isBlocked=0;
             if(MasterDataStructure.Mailbox[index].security != 2){
                  for (msg_Index = 0; msg_Index<10;msg_Index++)
                        {  
                      if(MasterDataStructure.Mailbox[index].Message[msg_Index].Reciever==RecieverprocessId){
			    SenderprocessId=MasterDataStructure.Mailbox[index].Message[msg_Index].Sender;
                            printf("\nMessage recieved :%s from %d, Recieved by %d", MasterDataStructure.Mailbox[index].Message[msg_Index].Message,SenderprocessId,RecieverprocessId);
                            strncpy(MasterDataStructure.Mailbox[index].Message[msg_Index].Message, "\0", M3_STRING-1);
							MasterDataStructure.Mailbox[index].Message[msg_Index].Reciever=0;
                            MasterDataStructure.Mailbox[index].Max_msg_index--;
                      }
                           
                      MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].unread_Msg--;
                  }
                 
             
             
             }
             else{
             
                 printf("Mailbox is Read Protected");
                 return EC_READ_PROTECTED;
             
             }
  

}

int do_DeleteMailBox(){
    
     int mailboxId=0;
    pid_t SenderprocessId;
    int index=0;
    int flag=0;
    int Max_msg_index=0;
    
  mailboxId= m_in.mbox_id;
  SenderprocessId=m_in.p_Sprocessid;
 
  for(index=0; index < num_of_mailbox; index++){
 
     if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
         flag++;
         break;
     }
 
 }
  
  if(flag==0){
      
        printf("\n Mail box does not exit\n");
        return EC_MAILBOX_DOESNT_EXIST;
  
  }
  
  if ( MasterDataStructure.Mailbox[index].ProcessInfo[0].processId==SenderprocessId)   //check if process is admin to the mailbox
        {         
       
      if(MasterDataStructure.Mailbox[index].security==0 || MasterDataStructure.Mailbox[index].security==2){
                    
              for(Max_msg_index=0; Max_msg_index < MasterDataStructure.Mailbox[index].Max_msg_index; Max_msg_index++){
              
                  strcpy( MasterDataStructure.Mailbox[index].Message[Max_msg_index].Message,"\0");
              }
            
              MasterDataStructure.Mailbox[index].MailBoxId=0;
              MasterDataStructure.Mailbox[index].Max_msg_index=0;
              num_of_mailbox--;
            
              printf("\nMailbox %d removed sucessfully at Index :%d\n",mailboxId, index);
              return EC_OK;
              
      }
      else{
      
          printf("\nCannot Delete Mailbox. Security Violation\n");
          return EC_CANNOT_DELETE;
      }
                   
   }
  else{
  printf("\nCannot Delete Mailbox. User Is Not an Admin\n");
          return EC_CANNOT_DELETE;
  
  }

}

int do_RegMailBox(){

    int mailboxId=0;
    pid_t SenderprocessId;
    int index=0;
    int flag=0;
    int pIndex=0;
    int pid=0;
    int flag1=0;
     
    mailboxId= m_in.mbox_id;
    SenderprocessId=m_in.p_Sprocessid;
    pid=m_in.p_Rprocessid;
    
   for(index=0; index < num_of_mailbox; index++){
     if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
         flag++;
         break;
     }
 
 }
  
  if(flag==0){
      
        printf("\n Mail box does not exit\n");
        return EC_MAILBOX_DOESNT_EXIST;
  
  }
  

        for(pIndex=0; pIndex < MasterDataStructure.Mailbox[index].LatestProcess; pIndex++){
        
            if(MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId==pid){
                
                printf("\nProcess is already part of Mailbox");
                return EC_PROCESS_EXIST;
            
            }
        
        }
  
    for(pIndex=0; pIndex < MasterDataStructure.Mailbox[index].LatestProcess; pIndex++){
        
            if(MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId==SenderprocessId){
                
                 if (MasterDataStructure.Mailbox[index].LatestProcess < NUMBER_OF_PROCESSES){
                 pIndex=MasterDataStructure.Mailbox[index].LatestProcess; 
                 MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId=pid;
                 MasterDataStructure.Mailbox[index].LatestProcess++;
                 flag1++;
                 printf("\nSuccessfully Registered process %d with Mailbox %d",pid,mailboxId);
                 return EC_OK;
  
                 }
            }
        
        }
    if(flag1==0){
    
        printf("\nYou do not have permissions to add a process\n");
    }
    

}

int do_Security(){
 int mailboxId=0;
    pid_t processId;
    int securityBit=0;
    int index=0;
    int flag=0;
    int pIndex;
    int processflag=0;
  mailboxId= m_in.mbox_id;
  processId= m_in.p_Sprocessid;
  securityBit= m_in.m_SecurityBit;
  
  for(index=0; index < num_of_mailbox; index++){
 
     if(MasterDataStructure.Mailbox[index].MailBoxId==mailboxId){
         flag++;
         break;
     }
 
 }
  
  if(flag==0){
      
        printf("\n Mail box does not exit\n");
        return EC_MAILBOX_DOESNT_EXIST;
  
  }
   
  for(pIndex=0; pIndex< MasterDataStructure.Mailbox[index].LatestProcess;pIndex++)
   {
      
      if(MasterDataStructure.Mailbox[index].ProcessInfo[pIndex].processId==processId){
          processflag++;
          break;
      }
}
  if(processflag==0){
      printf("ProcessId %d is not admin of Mailbox %d\n",processId,mailboxId);
      return EC_PROCESS_DOESNT_EXIST;
  }
    
     MasterDataStructure.Mailbox[index].security=securityBit;
     printf("Successfully changed the security of mailbox");
  
     return EC_OK;
  

}