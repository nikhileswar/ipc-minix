/* 
 * File:   IPC.h
 * Author: nikhil
 *
 * Created on March 24, 2014, 11:11 PM
 */


#include <lib.h>


#define NUMBER_OF_MESSAGES 10   //number of messages in mailbox
#define NUMBER_OF_MAILBOX 5    //number of mailbox
#define NUMBER_OF_PROCESSES 10    //number of processes
#define LENGTH_OF_MESSAGE 10    //number of processes

/*Messages Definitions*/

#define m_msg m3_cal       /*Not Used*/
#define m_msgBox m1_p2    
#define m_msg_length m1_i1  
#define mbox_id m1_i2        /*Mailbox Id*/
#define p_Sprocessid m1_i3    /*For Sender Process*/
#define p_Rprocessid m2_i1    /*For Reciever Process*/
#define m_SecurityBit m7_i4    /*Security Bit*/

//typedef struct Message *MessagePointer;

struct Message{
  char Message[LENGTH_OF_MESSAGE];
  int Length;
  pid_t Sender;    //sender processid
  pid_t Reciever;  //reciever processid
} Message[NUMBER_OF_MESSAGES];


struct ProcessInfo{
     pid_t processId; 
     int unread_Msg;
}ProcessInfo[NUMBER_OF_PROCESSES];


struct Mailbox{
    
    int MailBoxId;
    struct Message Message[NUMBER_OF_MESSAGES];
    struct ProcessInfo ProcessInfo[NUMBER_OF_PROCESSES];
    pid_t LatestProcess;
    int Max_msg_index;
    int security; /* 0= read-write 
                     1= read-only
                     2= write-only */
    int isBlocked;

}Mailbox[NUMBER_OF_MAILBOX];

struct MasterDataStructure{
    struct Mailbox Mailbox[NUMBER_OF_MAILBOX];

}MasterDataStructure;


